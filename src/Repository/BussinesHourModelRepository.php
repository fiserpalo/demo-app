<?php

namespace App\Repository;

use App\Entity\BranchModel;
use App\Entity\BussinesHourModel;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method BussinesHourModel|null find($id, $lockMode = null, $lockVersion = null)
 * @method BussinesHourModel|null findOneBy(array $criteria, array $orderBy = null)
 * @method BussinesHourModel[]    findAll()
 * @method BussinesHourModel[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BussinesHourModelRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BussinesHourModel::class);
    }

    // /**
    //  * @return BussinesHourModel[] Returns an array of BussinesHourModel objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BussinesHourModel
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

}
