<?php

namespace App\Repository;

use App\Entity\BranchModel;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method BranchModel|null find($id, $lockMode = null, $lockVersion = null)
 * @method BranchModel|null findOneBy(array $criteria, array $orderBy = null)
 * @method BranchModel[]    findAll()
 * @method BranchModel[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BranchModelRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BranchModel::class);
    }

    // /**
    //  * @return BranchModel[] Returns an array of BranchModel objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BranchModel
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
