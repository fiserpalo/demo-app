<?php

namespace App\Repository;

use App\Entity\CarrierModel;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CarrierModel|null find($id, $lockMode = null, $lockVersion = null)
 * @method CarrierModel|null findOneBy(array $criteria, array $orderBy = null)
 * @method CarrierModel[]    findAll()
 * @method CarrierModel[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CarrierModelRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CarrierModel::class);
    }

    // /**
    //  * @return CarrierModel[] Returns an array of CarrierModel objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CarrierModel
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
