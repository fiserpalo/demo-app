<?php

namespace App\Entity;

use App\Repository\BussinesHourModelRepository;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

/**
 * @ORM\Entity(repositoryClass=BussinesHourModelRepository::class)
 */
class BussinesHourModel
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\Column(type="uuid")
     * @Serializer\Type("uuid")
     * @Serializer\Groups({"branchDetail"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=31, nullable=true)
     * @Serializer\Groups({"branchDetail"})
     */
    private $dayOfWeek;

    /**
     * @ORM\Column(type="string", length=31, nullable=true)
     * @Serializer\Groups({"branchDetail"})
     */
    private $bussinesHour;

    public function __construct()
    {
        $this->id = Uuid::uuid4();
    }

    public function getId():  ?UuidInterface
    {
        return $this->id;
    }

    public function getDayOfWeek(): ?string
    {
        return $this->dayOfWeek;
    }

    public function setDayOfWeek(?string $dayOfWeek): self
    {
        $this->dayOfWeek = $dayOfWeek;

        return $this;
    }

    public function getBussinesHour(): ?string
    {
        return $this->bussinesHour;
    }

    public function setBussinesHour(?string $bussinesHour): self
    {
        $this->bussinesHour = $bussinesHour;

        return $this;
    }
}
