<?php

namespace App\Entity;

use App\Repository\BranchModelRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;


/**
 * @ORM\Entity(repositoryClass=BranchModelRepository::class)
 */
class BranchModel
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\Column(type="uuid")
     * @Serializer\Type("uuid")
     * @Serializer\Groups({"branch","branchDetail"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Serializer\Groups({"branch","branchDetail"})
     */
    private $internalId;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Serializer\Groups({"branch","branchDetail"})
     */
    private $internalName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Serializer\Groups({"branch","branchDetail"})
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Serializer\Groups({"branch","branchDetail"})
     */
    private $web;

    /**
     * @ORM\Column(type="string", length=511, nullable=true)
     * @Serializer\Groups({"branch","branchDetail"})
     */
    private $announcement;

    /**
     * @ORM\ManyToMany(targetEntity=BussinesHourModel::class)
     * @Serializer\Groups({"branchDetail"})
     */
    private $businessHours;

    /**
     * @ORM\ManyToOne(targetEntity=CarrierModel::class, inversedBy="branches")
     * @Serializer\Groups({"branchDetail"})
     */
    private $carrierModel;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $latitude;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $longitude;

    /**
     * @ORM\Column(type="array", nullable=true)
     * @Serializer\Groups({"branch","branchDetail"})
     */
    private $location;

    public function __construct()
    {
        $this->id = Uuid::uuid4();
        $this->businessHours = new ArrayCollection();
        $this->location = [];

    }

    public function getId(): ?UuidInterface
    {
        return $this->id;
    }

    public function getInternalId(): ?string
    {
        return $this->internalId;
    }

    public function setInternalId(?string $internalId): self
    {
        $this->internalId = $internalId;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getWeb(): ?string
    {
        return $this->web;
    }

    public function setWeb(?string $web): self
    {
        $this->web = $web;

        return $this;
    }

    public function getAnnouncement(): ?string
    {
        return $this->announcement;
    }

    public function setAnnouncement(?string $announcement): self
    {
        $this->announcement = $announcement;

        return $this;
    }

    /**
     * @return Collection|BussinesHourModel[]
     */
    public function getBusinessHours(): Collection
    {
        return $this->businessHours;
    }

    public function addBusinessHour(BussinesHourModel $businessHour): self
    {
        if (!$this->businessHours->contains($businessHour)) {
            $this->businessHours[] = $businessHour;
        }

        return $this;
    }

    public function removeBusinessHour(BussinesHourModel $businessHour): self
    {
        $this->businessHours->removeElement($businessHour);

        return $this;
    }

    public function getCarrierModel(): ?CarrierModel
    {
        return $this->carrierModel;
    }

    public function setCarrierModel(?CarrierModel $carrierModel): self
    {
        $this->carrierModel = $carrierModel;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getInternalName()
    {
        return $this->internalName;
    }

    /**
     * @param mixed $internalName
     */
    public function setInternalName($internalName): void
    {
        $this->internalName = $internalName;
    }

    public function getLatitude(): ?float
    {
        return $this->latitude;
    }

    public function setLatitude(?float $latitude): self
    {
        $this->latitude = $latitude;
        $this->setLocation();

        return $this;
    }

    public function getLongitude(): ?float
    {
        return $this->longitude;
    }

    public function setLongitude(?float $longitude): self
    {
        $this->longitude = $longitude;
        $this->setLocation();

        return $this;
    }

    function setLocation()
    {
        $this->location = [
            'lat' => $this->getLatitude(),
            'lng' => $this->getLongitude()
        ];
    }

    public function getLocation(): array
    {
        return $this->location;
    }
}
