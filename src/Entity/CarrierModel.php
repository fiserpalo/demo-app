<?php

namespace App\Entity;

use App\Repository\CarrierModelRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

/**
 * @ORM\Entity(repositoryClass=CarrierModelRepository::class)
 */
class CarrierModel
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\Column(type="uuid")
     * @Serializer\Type("uuid")
     * @Serializer\Groups({"branchDetail"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Serializer\Groups({"branchDetail"})
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Serializer\Groups({"branchDetail"})
     */
    private $url;

    /**
     * @ORM\OneToMany(targetEntity=BranchModel::class, mappedBy="carrierModel")
     */
    private $branches;

    public function __construct()
    {
        $this->branches = new ArrayCollection();
        $this->id = Uuid::uuid4();
    }

    public function getId(): ?UuidInterface
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|BranchModel[]
     */
    public function getBranches(): Collection
    {
        return $this->branches;
    }

    public function addBranch(BranchModel $branch): self
    {
        if (!$this->branches->contains($branch)) {
            $this->branches[] = $branch;
            $branch->setCarrierModel($this);
        }

        return $this;
    }

    public function removeBranch(BranchModel $branch): self
    {
        if ($this->branches->removeElement($branch)) {
            // set the owning side to null (unless already changed)
            if ($branch->getCarrierModel() === $this) {
                $branch->setCarrierModel(null);
            }
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     */
    public function setUrl($url): void
    {
        $this->url = $url;
    }
}
