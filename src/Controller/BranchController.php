<?php

namespace App\Controller;

use App\Entity\BranchModel;
use App\Repository\BranchModelRepository;
use App\Service\Branch\BranchModelService;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BranchController extends AbstractController
{


    /**
     * @var BranchModelService
     */
    private $branchSercice;

    /**
     * @var SerializerInterface
     */
    private $serializer;

    public function __construct(SerializerInterface $serializer, BranchModelService $branchSercice)
    {
        $this->serializer = $serializer;
        $this->branchSercice = $branchSercice;
    }

    /**
     * @Route("/api/branch/{page}", name="branch_index")
     * @param int $page
     * @return JsonResponse
     */
    public function index(int $page): JsonResponse
    {
        $ctx = SerializationContext::create()->setGroups(["branch"])->enableMaxDepthChecks();

        return new JsonResponse(
            $this->serializer->serialize(['branches' => $this->branchSercice->getAll($page)], 'json', $ctx),
            Response::HTTP_OK,
            [],
            true
        );

    }

    /**
     * @Route("/api/branch", name="branch_new", methods={"POST"})
     * @param Request $request
     * @return Response
     */
    public function new(Request $request): Response
    {
        $branchModel = $this->serializer->deserialize(
            $request->getContent(),
            BranchModel::class,
            'json'
        );

        $this->branchSercice->newBranchModel($branchModel);

        return new JsonResponse(
            $this->serializer->serialize($branchModel, 'json'),
            Response::HTTP_CREATED,
            [],
            true
        );
    }



    /**
     * @Route("/api/branch/{branchModel}", name="branch_edit", methods={"PUT"})
     * @param BranchModelRepository $branchModelRepository
     * @param Request $request
     * @param BranchModel $branchModel
     * @return RedirectResponse
     */
    public function edit(BranchModelRepository $branchModelRepository, Request $request, BranchModel $branchModel)
    {

        $data = $this->serializer->deserialize(
            $request->getContent(),
            BranchModel::class,
            'json'
        );

        $this->branchSercice->updateBranchModel($branchModel, $data);
        return $this->redirectToRoute("branch_detail", ["branchModel" => $branchModel]);

    }

    /**
     * @Route("/api/branch/{branchModel}", name="branch_remove", methods={"DELETE"})
     * @param BranchModel $branchModel
     * @return RedirectResponse
     */
    public function remove( BranchModel $branchModel): RedirectResponse
    {
        $this->branchSercice->removeBranchModel($branchModel);

        return $this->redirectToRoute("branch_index");
    }

    /**
     * @Route("/api/branch/{branchModel}", name="branch_detail")
     */
    public function detail(BranchModel $branchModel): JsonResponse
    {
        $ctx = SerializationContext::create()->setGroups(["branchDetail"])->enableMaxDepthChecks();

        return new JsonResponse(
            $this->serializer->serialize(['branch' => $branchModel], 'json', $ctx),
            Response::HTTP_OK,
            [],
            true
        );

    }

}
