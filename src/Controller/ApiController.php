<?php

namespace App\Controller;

use App\Entity\CarrierModel;
use App\Service\Carrier\CarrierService;
use JMS\Serializer\SerializerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class ApiController extends AbstractController
{


    /**
     * @var CarrierService
     */
    private $carrierService;

    /**
     * @var SerializerInterface
     */
    private $serializer;

    public function __construct(CarrierService $carrierService,SerializerInterface $serializer)
    {
        $this->carrierService = $carrierService;
        $this->serializer = $serializer;
    }

    /**
     * @Route("/api/load/{carrierModel}/", name="api_index")
     * @param CarrierModel $carrierModel
     * @return Response
     */
    public function index(CarrierModel $carrierModel): Response
    {

        $carrierModel =  $this->carrierService->loadNewData($carrierModel);

        return new JsonResponse(
            $this->serializer->serialize(['carrier' => $carrierModel], 'json'),
            Response::HTTP_OK,
            [],
            true
        );
    }




}
