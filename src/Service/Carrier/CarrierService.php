<?php

namespace App\Service\Carrier;

use App\Entity\BranchModel;
use App\Entity\BussinesHourModel;
use App\Entity\CarrierModel;
use App\Interfaces\CarrierInterface;
use App\Repository\BranchModelRepository;
use Doctrine\ORM\EntityManagerInterface;
use Nette\Utils\Json;
use Nette\Utils\JsonException;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;


class CarrierService implements CarrierInterface
{

    private $httpClient;


    /**
     * @var EntityManagerInterface
     */
    private $em;


    public function __construct(EntityManagerInterface $em)
    {
        $this->httpClient = HttpClient::create();
        $this->em = $em;
    }


    /**
     * @param string $url
     * @return mixed
     * @throws TransportExceptionInterface
     * @throws JsonException
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     */
    public function loadRespense(string $url)
    {
        $response = $this->httpClient->request('GET', $url);
        return Json::decode($response->getContent());
    }

    /**
     * @return CarrierModel
     */
    public function getDefaultCarrier(): CarrierModel
    {
        return $this->getCarrier("2ef8cbba-7806-4cff-ae97-35094262a76b");
    }

    /**
     * @param $idCarrier
     * @return CarrierModel
     */
    public function getCarrier($idCarrier): CarrierModel
    {
        return $this->em->getRepository(CarrierModel::class)->findOneBy(['id' => $idCarrier]);
    }


    public function loadNewData(CarrierModel $carrierModel)
    {
        $dataFromApi = $this->loadRespense($carrierModel->getUrl());

        $branchModelRepo = $this->em->getRepository(BranchModel::class);

        foreach ($dataFromApi as $branchFromApi) {


            /** @var BranchModelRepository $branchModelRepo */
            $branch = $branchModelRepo->findOneBy(["internalId" => $branchFromApi->id]);

            if ($branch != null) {
                $this->removeHoursFromBranch($branch);
                $this->addHoursToBranch($branchFromApi->openingHours, $branch);
            } else {
                $branch = new BranchModel();
                $this->addHoursToBranch($branchFromApi->openingHours, $branch);
            }

            $branch->setCarrierModel($carrierModel);
            $carrierModel->addBranch($branch);
            $this->updateBranch($branchFromApi, $branch);
            $this->em->persist($branch);

        }
        $this->em->flush();
        return $carrierModel;
    }


    private function removeHoursFromBranch(BranchModel $branch)
    {
        $businessHours = $branch->getBusinessHours();
        foreach ($businessHours as $businessHour) {
            $this->em->remove($businessHour);
        }
    }

    private function addHoursToBranch($businessHours, BranchModel $branch)
    {
        foreach ($businessHours as $businessHour) {
            $newBussinesHour = new BussinesHourModel();
            $newBussinesHour->setDayOfWeek((string)$businessHour->day);
            $newBussinesHour->setBussinesHour((string)($businessHour->open . ' - ' . $businessHour->close));
            $branch->addBusinessHour($newBussinesHour);
            $this->em->persist($newBussinesHour);
        }
    }

    private function updateBranch($branchFromApi, BranchModel $branch)
    {
        $branch->setWeb($branchFromApi->odkaz);
        $branch->setAddress($branchFromApi->odkaz);
        $branch->setInternalName($branchFromApi->name);
        $branch->setInternalId($branchFromApi->id);
        $branch->setLatitude($branchFromApi->lat);
        $branch->setLongitude($branchFromApi->lng);
    }

}