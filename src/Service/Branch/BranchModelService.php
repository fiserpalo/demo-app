<?php


namespace App\Service\Branch;


use App\Entity\BranchModel;
use App\Entity\CarrierModel;
use Doctrine\ORM\EntityManagerInterface;

class BranchModelService
{

    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }


    public function newBranchModel(BranchModel $branchModel)
    {
        $this->entityManager->persist($branchModel);
        $this->entityManager->flush();
    }

    /**
     * @param BranchModel $branchModel
     * @param $data
     */
    public function updateBranchModel(BranchModel $branchModel, $data)
    {
        $branchModel->setLongitude($data->getLongitude());
        $branchModel->setLatitude($data->getLatitude());
        $branchModel->setAddress($data->getAddress());
        $branchModel->setWeb($data->getWeb());
        $branchModel->setAnnouncement($data->getAnnouncement());
        $branchModel->setInternalName($data->getInternalName());
        $branchModel->setCarrierModel($data->getCarrierModel());

        $this->entityManager->flush();

    }

    /**
     * @param BranchModel $branchModel
     */
    public function  removeBranchModel(BranchModel $branchModel)
    {
        $this->entityManager->remove($branchModel);
        $this->entityManager->flush();
    }

    /**
     * @param int $page
     * @return array
     */
    public function getAll(int $page = 1): array {
        return $this->entityManager->getRepository(BranchModel::class)->findBy([],null,30*$page,30*($page-1));
    }


}