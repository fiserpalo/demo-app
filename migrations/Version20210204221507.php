<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210204221507 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE branch_model (id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', carrier_model_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', internal_id VARCHAR(255) DEFAULT NULL, internal_name VARCHAR(255) DEFAULT NULL, address VARCHAR(255) DEFAULT NULL, web VARCHAR(255) DEFAULT NULL, announcement VARCHAR(511) DEFAULT NULL, latitude DOUBLE PRECISION DEFAULT NULL, longitude DOUBLE PRECISION DEFAULT NULL, location LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', INDEX IDX_B924708E1268D63 (carrier_model_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE branch_model_bussines_hour_model (branch_model_id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', bussines_hour_model_id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', INDEX IDX_139CA779ED66E24 (branch_model_id), INDEX IDX_139CA7710595294 (bussines_hour_model_id), PRIMARY KEY(branch_model_id, bussines_hour_model_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE bussines_hour_model (id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', day_of_week VARCHAR(31) DEFAULT NULL, bussines_hour VARCHAR(31) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE carrier_model (id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', name VARCHAR(255) DEFAULT NULL, url VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE refresh_tokens (id INT AUTO_INCREMENT NOT NULL, refresh_token VARCHAR(128) NOT NULL, username VARCHAR(255) NOT NULL, valid DATETIME NOT NULL, UNIQUE INDEX UNIQ_9BACE7E1C74F2195 (refresh_token), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE branch_model ADD CONSTRAINT FK_B924708E1268D63 FOREIGN KEY (carrier_model_id) REFERENCES carrier_model (id)');
        $this->addSql('ALTER TABLE branch_model_bussines_hour_model ADD CONSTRAINT FK_139CA779ED66E24 FOREIGN KEY (branch_model_id) REFERENCES branch_model (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE branch_model_bussines_hour_model ADD CONSTRAINT FK_139CA7710595294 FOREIGN KEY (bussines_hour_model_id) REFERENCES bussines_hour_model (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE branch_model_bussines_hour_model DROP FOREIGN KEY FK_139CA779ED66E24');
        $this->addSql('ALTER TABLE branch_model_bussines_hour_model DROP FOREIGN KEY FK_139CA7710595294');
        $this->addSql('ALTER TABLE branch_model DROP FOREIGN KEY FK_B924708E1268D63');
        $this->addSql('DROP TABLE branch_model');
        $this->addSql('DROP TABLE branch_model_bussines_hour_model');
        $this->addSql('DROP TABLE bussines_hour_model');
        $this->addSql('DROP TABLE carrier_model');
        $this->addSql('DROP TABLE refresh_tokens');
    }
}
